import React, {useState, useEffect} from 'react';
import {View, Text, FlatList, StyleSheet, Alert} from 'react-native';
import Header from './component/Header';
import ListItem from './component/ListItem';
import AddItem from './component/AddItem';
const App = () => {
  const [items, setItems] = useState([
    {id: '1', title: 'Milk'},
    {id: '2', title: 'Coffe'},
    {id: '3', title: 'Bread'},
    {id: '4', title: 'Donate'},
  ]);

  const deleteItem = (id) => {
    return setItems((prevItems) => prevItems.filter((item) => item.id !== id));
  };

  const addItem = (title) => {
    if (!title) {
      Alert.alert('Error', 'please enter a valid text');
    } else {
      return setItems((prevItems) => {
        return prevItems.concat([{id: `${Math.random()}`, title}]);
      });
    }
  };
  useEffect(() => {
    console.log(items);
  }, [items]);

  return (
    <View style={style.container}>
      <Header title="shoppingList" />
      <AddItem addItem={addItem} />
      <FlatList
        data={items}
        renderItem={({item}) => (
          <ListItem item={item} deleteItem={deleteItem} />
        )}
      />
    </View>
  );
};

export default App;
const style = StyleSheet.create({
  container: {
    flex: 1,
  },
});
